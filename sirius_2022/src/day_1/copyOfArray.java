package day_1;

import java.util.*;

public class copyOfArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i=0;i<n;i++) {
			a[i] = sc.nextInt();
		}
		int arrayCopy[] = Arrays.copyOf(a,10);
		for(int i=0;i<arrayCopy.length;i++) {
			System.out.println(arrayCopy[i]+" ");
		}
	}

}
