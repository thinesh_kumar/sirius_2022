package day_1;

import java.util.Scanner;

public class largeElementInRow {
	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	int n = sc.nextInt();
	int m = sc.nextInt();
	int a[][] = new int [n][m];
	for(int i=0;i<n;i++) {
		for(int j=0;j<m;j++) {
			a[i][j] = sc.nextInt();
		}
	}
	int largeNumber[] = new int [n];
	int max = 0;
	for(int i=0;i<n;i++) {
		for(int j=0;j<n;j++) {
			if(a[i][j] > max) {
				max = a[i][j];
			}
		}
		largeNumber[i] = max;
		max = 0;
	}
	for(int i=0;i<largeNumber.length;i++) {
		System.out.println(largeNumber[i]+" ");
	}
	}
}
