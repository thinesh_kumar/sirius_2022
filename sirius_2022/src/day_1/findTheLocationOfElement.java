package day_1;

import java.util.Scanner;

public class findTheLocationOfElement {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n =sc.nextInt();
		int a[] = new int[n];
		for(int i=0;i<n;i++) {
			a[i] = sc.nextInt();
		}
		int pos = sc.nextInt();
		System.out.println("The element is need to found is in index : ");
		for(int i=0;i<n;i++) {
			if(a[i] == pos) {
				System.out.println(i);
			}
		}
	}

}
