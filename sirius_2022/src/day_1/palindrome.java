package day_1;
import java.util.*;
public class palindrome {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);	
		int n = sc.nextInt();
		int ans=0;
		int originalNum = n;
		while(n>0) {
			int r = n%10;
			ans = ans*10+r;
			n=n/10;
		}
		if(originalNum == ans) {
			System.out.println("It is palindrome");
		}
		else System.out.println("Not a palindrome");
}
}
