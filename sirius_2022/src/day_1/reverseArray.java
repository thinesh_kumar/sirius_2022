package day_1;

import java.util.Scanner;

public class reverseArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i=0;i<n;i++) {
			a[i] = sc.nextInt();
		}
		System.out.println("The array before reversed");
		for(int i=0;i<n;i++) {
			System.out.print(a[i]+" ");
		}
		System.out.println("The array after reversed");
		for(int i=n-1;i>=0;i--) {
			System.out.print(a[i]+" ");
		}
	}

}
