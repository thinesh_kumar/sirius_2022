package day_1;
import java.util.*;
public class sortArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i=0;i<n;i++) {
			a[i] = sc.nextInt();
		}
		
		for(int i=0;i<n;i++) {
			int temp = 0;
			for(int j=0;j<n;j++) {
				if(a[i] < a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		System.out.print("The sorted array is : ");
		for(int i=0;i<n;i++) {
			System.out.print(a[i]+" ");
		}
	}

}
