package day_1;

class Specifier{
	public void publicPrint() {
		System.out.println("This is public");
	}
	private void privatePrint() {
		System.out.println("This is private");
	}
	protected void protectedPrint() {
		System.out.println("This is protected");
	}
//	default void defaultPrint() {
//		System.out.println("This is default");
//	} This is used in interface.
}

public class accessSpecifiers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Specifier s = new Specifier();
		s.publicPrint();
//		s.privatePrint(); This is out of scope
		s.protectedPrint();
//		s.defaultPrint(); Default is for interface
		
	}

}
