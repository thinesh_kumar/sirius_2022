package day_1;

public class Implict {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Implicit
		byte num = 100;
		short s = num;
		int i = s;
		long l = i;
		float f = l;
		double d = f;
		System.out.println("Byte :"+num);
		System.out.println("Short :"+s);
		System.out.println("int : "+i);
		System.out.println("long :"+l);
		System.out.println("float :"+f);
		System.out.println("double : "+d);
		
		//Explicit 
		double D = 100;
		float F = (float) D;
		long L = (long) F;
		int I = (int)L;
		short S = (short)I;
		byte B = (byte)S;
		System.out.println("double : "+D);
		System.out.println("float :"+F);
		System.out.println("long :"+L);
		System.out.println("int : "+I);
		System.out.println("Short :"+S);
		System.out.println("Byte :"+B);
	}

}
