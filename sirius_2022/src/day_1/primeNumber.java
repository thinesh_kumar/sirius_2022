package day_1;
import java.util.*;
public class primeNumber {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int n = num/2;
		int flag = 0;
		if(num == 0 || num == 1) {
			System.out.println("It is not a prime number");
		}
		else {
			for(int i=0;i<=n;i++) {
				if(num%i == 0) {
					System.out.println("It is not a prime number");
					flag = 1;
					break;
				}
			}
		}
		if(flag == 0) {
			System.out.println("It is a prime number");
		}
	}
}
