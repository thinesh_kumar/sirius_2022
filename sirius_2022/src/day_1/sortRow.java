package day_1;
import java.util.*;
public class sortRow {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int m = sc.nextInt();
		int a[][] = new int [n][m];
		for(int i=0;i<n;i++) {
			for(int j=0;j<m;j++) {
				a[i][j] = sc.nextInt();
			}
		}
//		int start = 0;
//		int end = m-1;
		for(int i=0;i<n;i++) {
			for(int start = 0,end = m-1;start<end;start++,end--) {
				int temp = a[i][start];
				a[i][start] = a[i][end];
				a[i][end] = temp;
			}
		}
		for(int i=0;i<n;i++) {
			for(int j=0;j<m;j++) {
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
	}
}
