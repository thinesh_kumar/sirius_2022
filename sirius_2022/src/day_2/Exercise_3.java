package day_2;

public class Exercise_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Adder a = new Adder();
		int method1 = a.add(5, 10);
		double method2 = a.add(1, 2);
		System.out.println("The first method ans is "+method1);
		System.out.println("The second method ans is "+method2);
	}

}
class Adder{
	int add(int a,int b) {
		int ans = a+b;
		return ans;
	}
	double add(int a,int b) {
		double ans = a+b;
		return ans;
	}
}
