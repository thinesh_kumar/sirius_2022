package day_2;

public class Exercise_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AddOperation a = new AddOperation();
		int method1 = a.add(5, 10);
		double method2 = a.add(1.01, 2.98);
		System.out.println("The first method ans is "+method1);
		System.out.println("The second method ans is "+method2);
	}

}
 class AddOperation{
	int add(int a,int b) {
		int ans = a+b;
		return ans;
	}
	double add(double a,double b) {
		double ans = a+b;
		return ans;
	}
}
