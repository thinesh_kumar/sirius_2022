package day_2;

public class Exercise_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//A a = new A();
		A a =new B();
		B b = new B();
		a.sum(5,7);
		System.out.println(a.i+ " "+b.j);
	}

}
class A{
	int i = 10;
	public void sum(int a, int b) {
		int c = a+b;
		System.out.println("The sum of method1 "+c);
	}
	public A() {
		System.out.println("parent const"+i);
	}
	
}
class B extends A{
	//int i = 30;
	int j = 20;
	public void sum(int a, int b) {
		int c = a+b;
		System.out.println("The sum of method2 "+c);
	}
	public B() {
		i = 30;
		System.out.println("child const"+i+" "+j);
	}
}