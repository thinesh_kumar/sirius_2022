package day_2;

public class Exercise_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Addition a = new Addition();
		int method1 = a.add(5, 10);
		int method2 = a.add(1, 2, 3);
		System.out.println("The first method ans is "+method1);
		System.out.println("The second method ans is "+method2);
	}

}
class Addition{
	int add(int a,int b) {
		int ans = a+b;
		return ans;
	}
	int add(int a,int b,int c) {
		int ans = a+b+c;
		return ans;
	}
}
